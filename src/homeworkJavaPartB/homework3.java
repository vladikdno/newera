package homeworkJavaPartB;

public class homework3 {
    public static void main(String[] args) {
        //3. Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат вывести в консоль.

        String text = ("Today after work, i'm gonna walk with my girlfriend. And after walk, i'll start to finish our homework.");
        int countSpaces = text.length() - text.replaceAll(" ", "").length();    // text.length() - text.replaceAll("[^\\s]"
        //int countSentences = text.length() - text.replaceAll(".", "").length();
        int countTest = text.length() - text.replace(".", "").length();     //не путать replace() и replaceAll()
        System.out.println("Count of spaces = " + countSpaces);
        System.out.println("Count of sentences = " + countTest);

    }
}
