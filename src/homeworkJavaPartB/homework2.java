package homeworkJavaPartB;

public class homework2 {
    public static void main(String[] args) {
        //2. Дано три различных числа (a, b, c). Решить квадратное уравнение ax^2 + bx + c = 0. Результат вывести в консоль.

        double a, b, c;
        double D;
        a = 81;
        b = 64;
        c = 100;
        D = b * b - (4 * a * c);
        if (D > 0) {
            double x1, x2;
            x1 = (-b + Math.sqrt(D))/ (2 * a);
            x2 = (b - Math.sqrt(D))/ (2 * a);
            System.out.println("Root of equation x1 = " + x1 + ", x2 = " + x2);
        } else if (D == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Equation got only 1 root x = " + x);
        } else {
            System.out.println("Equation has no active roots!");
        }
    }
}
