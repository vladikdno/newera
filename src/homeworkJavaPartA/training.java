package homeworkJavaPartA;

public class training {
    public static void main(String[] args) {

        //int x = 5; // 1. Объявить переменную типа int, присвоить ей значение 5 и вывести её в консоль.
        // System.out.println(x);

        //int x = 5; // 2. Объявить переменную типа int, присвоить ей любое значение, увеличить ее на 2, результат вывести в консоль.
        //System.out.print(x*2);

        // int x = 20; // 3. Объявить переменную типа int, присвоить ей любое значение, уменьшить ее на 4, результат вывести в консоль.
        // System.out.print(x/4);

        // int x = 7; // 4. Объявить переменную типа int, присвоить ей любое значение, умножить ее на 6, результат вывести в консоль.
        // int res  = x*4;
        // System.out.print(res);

        // long x = 17L; // 5. Объявить переменную типа long, присвоить ей любое значение, разделить ее на 2, результат вывести в консоль.
        // long res = x/2;
        // System.out.print(res);

        // long x = 37L; // 6. Объявить переменную типа long, присвоить ей любое значение, найти и вывести в консоль остаток от деления на 5.
        // x = x % 5;
        // System.out.print(x);

        // float x = 28F; // 7. Объявить переменную типа float, присвоить ей любое значение, разделить ее на 3 и прибавить 6. Результат вывести в консоль.
        // float resX = x / 3 + 6;
        // System.out.print(resX);

        // double x = 95; // 8. Объявить переменную типа double, присвоить ей любое значение, отнять от нее 5, результат умножить на 6 и вывести в консоль.
        // double resX = (95-5) * 6;
        //System.out.print(resX);

        // int x = 3; // 9. Объявить целочисленную переменную, увеличить ее на 1 тремя разными способами. Результат вывести в консоль.
        // x = x + 1;
        // System.out.println(x);
        // ++x;
        // System.out.println(x);
        // x++;
        // System.out.println(x);

        // int x = 8; // 10. Объявить целочисленную переменную, уменьшить ее на 1 тремя разными способами. Результат вывести в консоль.
        // x = x - 1;
        // System.out.println(x);
        // x--;
        // System.out.println(x);
        // --x;
        // System.out.println(x);
        //starts init variables
        int a = 5;
        int b = 10;

        //find max of numbers and print square of it
        if (a > b) {
            System.out.println("a^2 = " + a * a);
        } else {
            System.out.println("b^2 = " + b * b);
        }
    }
}
