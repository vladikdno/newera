package homeworkJavaPartA;

public class PartA {
    public static void main(String[] args) {

        String russia = "Population of Russia is ";           // 1. Объявить несколько переменных, содержащих население европейских стран (3-5 штук)
        String belgium = "Population of Belgium is ";         // Вывести в консоль информацию по странам.
        String netherlands = "Population of Netherlands is "; // Например: "Германия: 80620000 человек".
        String france = "Population of France is ";
        String uk = "Population of UK is ";

        int populationRussia = 143_964_709;
        int populationBelgium = 11_547_820;
        int populationNetherlands = 17_121_639;
        int populationFrance = 65_233_271;
        int populationUK = 66_573_504;

        System.out.println(russia + populationRussia);
        System.out.println(uk + populationUK);
        System.out.println(france + populationFrance);
        System.out.println(netherlands + populationNetherlands);
        System.out.println(belgium + populationBelgium);
    }
}