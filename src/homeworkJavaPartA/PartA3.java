package homeworkJavaPartA;

public class PartA3 {
    public static void main(String[] args) {

        long daniaSquare = 43_094L;
        long franceSquare = 547_030L;       // 2. Объявить несколько переменных, хранящих площадь нескольких стран мира (3-5 штук) в кв. километрах.
        long russiaSquare = 17_075_400L;    // Рассчитать площадь в кв. метрах и кв. милях. Результат вывести в консоль.
        long germanySquare = 357_022L;
        long austriaSquare = 83_871L;

        long inMetres = 1_000_000L;
        double inMiles = 0.386;

        String squareOf = "Square of ";
        String metres = " in metres";
        String miles = " in miles";

        System.out.println(squareOf + "Dania " + daniaSquare * inMetres + metres);
        System.out.println(squareOf + "Dania " + daniaSquare * inMiles + miles);
        System.out.println(squareOf + "France " + franceSquare * inMetres + metres);
        System.out.println(squareOf + "France " + franceSquare * inMiles + miles);
        System.out.println(squareOf + "Russia " + russiaSquare * inMetres + metres);
        System.out.println(squareOf + "Russia " + russiaSquare * inMiles + miles);
        System.out.println(squareOf + "Germany " + germanySquare * inMetres + metres);
        System.out.println(squareOf + "Germany " + germanySquare * inMiles + miles);
        System.out.println(squareOf + "Austria " + austriaSquare * inMetres + metres);
        System.out.println(squareOf + "Austria " + austriaSquare * inMiles + miles);
    }
}
