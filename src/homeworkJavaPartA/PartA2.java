package homeworkJavaPartA;

public class PartA2 {
    public static void main(String[] args) {

        double usd = 27.011;
        double eur = 30.424;
        double rub = 0.422;      // 3. Объявить несколько переменных, содержащих текущий курс валют (3-4 штуки).
        double pln = 7.151;      // Сконвертировать 5000 грн в валюту по курсу, указанному в переменных для каждой валюты.
        double gbp = 35.753;     // Результат вывести в консоль.
        double uah = 5000;

        double resUSD = uah / usd;
        double resEur = uah / eur;
        double resRub = uah / rub;
        double resPln = uah / pln;
        double resGbp = uah / gbp;

        String fiveThousands = "5k UAH at USD = ";

        System.out.println(fiveThousands + resUSD);
        System.out.println(fiveThousands + resEur);
        System.out.println(fiveThousands + resRub);
        System.out.println(fiveThousands + resPln);
        System.out.println(fiveThousands + resGbp);
    }
}